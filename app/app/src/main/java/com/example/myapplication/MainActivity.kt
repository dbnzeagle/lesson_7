package com.example.myapplication

import android.Manifest
import android.app.Activity
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val localDatabase = LocalDatabase(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.addUserMenuItem -> {
                val intent = Intent(this, FormCreateUserActivity::class.java)
                startActivity(intent)
                true
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun initViews() {

        val users = localDatabase.getUsers()
        val userAdapter = UserAdapter(this, users, onUserClick = { userId ->
            val intent = Intent(this, FormUserActivity::class.java)
            intent.putExtra("user_id", userId)
            startActivity(intent)
        })

        usersRecyclerView.adapter = userAdapter
        usersRecyclerView.layoutManager = LinearLayoutManager(this)
        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, AlarmReceiver::class.java)
        intent.action = "RECEIVER_ACTION"
        val pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)
        alertManager.setOnClickListener {

            val delay = 1
            val alarmTime = System.currentTimeMillis() + delay * 2 * 1000L
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, alarmTime, pendingIntent)
//            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,alarmTime,AlarmManager.INTERVAL_FIFTEEN_MINUTES,pendingIntent)

        }
        contentProvider.setOnClickListener {
            requestPermission()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                alarmManager.cancel(pendingIntent)
            }
        }
    }

    fun requestPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                //TODO Вызов диалога для получения разрешения на использование чего либо
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle("ReadContacts")
                alertDialog.setPositiveButton(
                    android.R.string.ok
                ) { dialog, which ->
                    val contactUri = ContactsContract.Contacts.CONTENT_URI
                    val cursor = contentResolver
                        .query(contactUri, null, null, null, null)
                    Log.i("myLogs", cursor.toString())
                }
                alertDialog.setMessage("Pleaszse enable access to read contacts")
                alertDialog.setOnDismissListener {
                    Log.i("myLogs", "Error")

                }
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    1
                )

            }

        }

    }

    private fun updateCountUserText() {
        val countUser = localDatabase.getUsers().count()
        countUserTextView.text = "Количество пользователей: $countUser"
    }

    private fun refreshUsers() {
        val users = localDatabase.getUsers()
        val adapter = usersRecyclerView.adapter as UserAdapter
        adapter.users = users
        adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        refreshUsers()
        updateCountUserText()
        super.onResume()
    }
}
