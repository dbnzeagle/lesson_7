package com.example.myapplication

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_user_recycler_view.view.*

class UserAdapter(
    private val context: Context,
    var users: ArrayList<User>,
    val onUserClick: (Int) -> Unit
) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bindUser(user: User, onUserClick: (Int) -> Unit) {
            itemView.apply {
                userNameTextView.text = user.fio
                userIdTextView.text = user.id?.toString()
                userCityTextView.text = user.address
                setOnClickListener { onUserClick.invoke(user.id ?: 1) }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_user_recycler_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindUser(user = users[position], onUserClick = onUserClick)
    }
}