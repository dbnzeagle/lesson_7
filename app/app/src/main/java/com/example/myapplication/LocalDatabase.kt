package com.example.myapplication

import android.content.ContentValues
import android.content.Context
import android.provider.BaseColumns

class LocalDatabase(context: Context) : ILocalDatabase {

    private val dbHelper = DatabaseHelper(context = context)
    override fun insertUser(user: User): Long? {
        val db = dbHelper.writableDatabase

        val values = ContentValues().apply {
            put(DatabaseContract.UserEntity.COLUMN_NAME_FIO, user.fio)
            put(DatabaseContract.UserEntity.COLUMN_NAME_ADDRESS, user.address)
            put(DatabaseContract.UserEntity.COLUMN_NAME_GENDER, user.gender)
            put(DatabaseContract.UserEntity.COLUMN_NAME_PHONE_NUMBER, user.phoneNumber)
            put(DatabaseContract.UserEntity.COLOR, user.color)
        }

        val newRowId = db?.insert(DatabaseContract.UserEntity.TABLE_NAME, null, values)
        db?.close()
        return newRowId
    }

    override fun getUsers(): ArrayList<User> {
        val db = dbHelper.readableDatabase

        val cursor = db.query(
            DatabaseContract.UserEntity.TABLE_NAME,
            null,
            null,
            null,
            null,
            null,
            null
        )

        cursor.moveToFirst()

        val userList: ArrayList<User> = arrayListOf()

        while (!cursor.isAfterLast) {
            val id = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))
            val fio =
                cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_FIO))
            val address =
                cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_ADDRESS))
            val phoneNumber =
                cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_PHONE_NUMBER))
            val gender =
                cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_GENDER))
            val color = cursor.getInt(cursor.getColumnIndex(DatabaseContract.UserEntity.COLOR))
            val user = User(id, fio, address, phoneNumber, gender, color)
            userList.add(user)

            cursor.moveToNext()
        }

        cursor.close()
        db.close()
        return userList
    }

    override fun getUserById(id: Int): User {
        val db = dbHelper.readableDatabase

        val selection = "${BaseColumns._ID} = ?"
        val selectionArgs = arrayOf(id.toString())
        val cursor = db.query(
            DatabaseContract.UserEntity.TABLE_NAME,
            null,
            selection,
            selectionArgs,
            null,
            null,
            null
        )

        cursor.moveToFirst()

        val userId = cursor.getInt(cursor.getColumnIndex(BaseColumns._ID))
        val fio =
            cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_FIO))
        val address =
            cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_ADDRESS))
        val phoneNumber =
            cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_PHONE_NUMBER))
        val gender =
            cursor.getString(cursor.getColumnIndex(DatabaseContract.UserEntity.COLUMN_NAME_GENDER))
        val color = cursor.getInt(cursor.getColumnIndex(DatabaseContract.UserEntity.COLOR))

        val user = User(userId, fio, address, phoneNumber, gender, color)
        cursor.close()
        db.close()
        return user
    }

    override fun updateUser(user: User): Int {
        val db = dbHelper.writableDatabase

        val values = ContentValues().apply {
            put(DatabaseContract.UserEntity.COLUMN_NAME_GENDER, user.gender)
            put(DatabaseContract.UserEntity.COLUMN_NAME_PHONE_NUMBER, user.phoneNumber)
            put(DatabaseContract.UserEntity.COLUMN_NAME_ADDRESS, user.address)
            put(DatabaseContract.UserEntity.COLUMN_NAME_FIO, user.fio)
            put(DatabaseContract.UserEntity.COLOR, user.color)
        }

        val whereCause = "${BaseColumns._ID} = ?"
        val whereArgs = arrayOf(user.id.toString())
        val updateRows =
            db.update(DatabaseContract.UserEntity.TABLE_NAME, values, whereCause, whereArgs)
        db.close()
        return updateRows
    }

    override fun deleteUsers(): Int? {
        val db = dbHelper.writableDatabase

        val deleteRows = db?.delete(DatabaseContract.UserEntity.TABLE_NAME, null, null)
        db.close()
        return deleteRows
    }

    override fun deleteUserById(id: Int): Int? {
        val db = dbHelper.writableDatabase

        val whereClause = "${BaseColumns._ID} = ?"
        val whereArgs = arrayOf(id.toString())

        val deleteRows = db?.delete(DatabaseContract.UserEntity.TABLE_NAME, whereClause, whereArgs)
        db?.close()
        return deleteRows
    }


}

interface ILocalDatabase {
    fun insertUser(user: User): Long?
    fun getUsers(): ArrayList<User>
    fun getUserById(id: Int): User
    fun updateUser(user: User): Int
    fun deleteUsers(): Int?
    fun deleteUserById(id: Int): Int?
}