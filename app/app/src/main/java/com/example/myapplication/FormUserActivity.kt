package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_form_user.*

class FormUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_user)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initViews(intent = intent)
    }

    private fun initViews(intent: Intent) {
        userDeleteButton.setOnClickListener {
            val localDatabase = LocalDatabase(this)
            localDatabase.deleteUserById(intent.getIntExtra("user_id", 1))
            Toast.makeText(this, "Пользователь удален", Toast.LENGTH_SHORT).show()
            finish()
        }

        userEditButton.setOnClickListener {
            val newIntent = Intent(this, FormCreateUserActivity::class.java)
            newIntent.putExtra("user_id", intent.getIntExtra("user_id", 1))
            startActivity(newIntent)
        }
    }

    private fun refreshUserInfo(intent: Intent) {
        val localDatabase = LocalDatabase(this)
        val user = localDatabase.getUserById(id = intent.getIntExtra("user_id", 1))

        userAddressTextView.text = "Адрес: ${user.address}"
        userFioTextView.text = "ФИО: ${user.fio}"
        userGenderTextView.text = "Пол: ${user.gender}"
        userPhoneNumberTextView.text = "Номер телефона: ${user.phoneNumber}"
    }

    override fun onResume() {
        refreshUserInfo(intent)
        super.onResume()
    }
}
