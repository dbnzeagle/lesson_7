package com.example.myapplication

data class User(
    var id: Int?,
    var fio: String,
    var address: String,
    var phoneNumber: String,
    var gender: String,
    var color: Int?
)