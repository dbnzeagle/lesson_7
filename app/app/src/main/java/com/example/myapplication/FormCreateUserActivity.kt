package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_form_create_user.*

class FormCreateUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form_create_user)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        initViews(intent = intent)
    }

    private fun initViews(intent: Intent) {
        setUserIfNeeded(intent = intent)
        userSaveButton.setOnClickListener {
            val localDatabase = LocalDatabase(this)
            val userIdByIntent = intent.getIntExtra("user_id", -1)
            val userId = if (userIdByIntent == -1) null else userIdByIntent

            val user = User(
                id = userId,
                fio = userFioEditText.text.toString(),
                address = userAddressEditText.text.toString(),
                phoneNumber = userPhoneNumberEditText.text.toString(),
                gender = userGenderEditText.text.toString(),
                color = null
            )

            if (user.id != null) {
                localDatabase.updateUser(user = user)
            } else {
                localDatabase.insertUser(user = user)
            }

            Toast.makeText(this, "Пользователь создан", Toast.LENGTH_LONG).show()
            finish()
        }
    }

    private fun setUserIfNeeded(intent: Intent) {
        val userIdByIntent = intent.getIntExtra("user_id", -1)
        if (userIdByIntent != -1) {
            val localDatabase = LocalDatabase(this)
            val user = localDatabase.getUserById(userIdByIntent)
            userAddressEditText.setText(user.address)
            userFioEditText.setText(user.fio)
            userPhoneNumberEditText.setText(user.phoneNumber)
            userGenderEditText.setText(user.gender)
        }
    }
}
