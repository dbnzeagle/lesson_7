package com.example.myapplication

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.provider.Settings.Global.getString
import android.util.Log
import androidx.core.app.NotificationCompat
import kotlin.random.Random

class AlarmReceiver : BroadcastReceiver() {

    val LOG_TAG = "myLogs"
    val channelId = "com.example.myapplication.notification"

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i(LOG_TAG, "Started")
        if (intent?.action == "RECEIVER_ACTION") {
            writeToDatabase(context)
        }
        if (intent?.action == "PUSH_ACTION") {
            Log.i(LOG_TAG, "АКТИВНОСТЬ")
        }
    }

    fun writeToDatabase(context: Context?) {
        Log.i(LOG_TAG, "In Func")
        val localDatabase = LocalDatabase(context!!)
        val userCount = localDatabase.getUsers().size
        val randomValue = Random.nextInt(1, userCount)
        Log.i(LOG_TAG, randomValue.toString())
        val user = localDatabase.getUserById(randomValue)
        val randomColor =
            Color.argb(255, Random.nextInt(256), Random.nextInt(256), Random.nextInt(256))
        val updatedUser =
            User(user.id, user.fio, user.address, user.phoneNumber, user.gender, randomColor)
        localDatabase.updateUser(updatedUser)
        Log.i(LOG_TAG, localDatabase.getUserById(randomValue).toString())

        callNotification(context, randomColor)
    }

    fun callNotification(context: Context?, color: Int) {
        val channel = createChannel()
        val actionIntent = Intent(context, AlarmReceiver::class.java).apply {
            action = "PUSH_ACTION"
        }
        val pendingIntent = PendingIntent.getBroadcast(context, 0, actionIntent, 0)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //TODO Для версий 8.0 и выше
            val notification =
                Notification.Builder(context, channelId)
                    .setSmallIcon(R.drawable.ic_launcher_foreground)
                    .setContentTitle("База данных")
                    .setContentText("Записали")
                    .addAction(R.drawable.floating, "Действие", pendingIntent)
                    .build()
            App.notificationManager.createNotificationChannel(channel)
            App.notificationManager.notify(1, notification)
        } else {
            //TODO Для версий 7.1 и ниже
            val notification = NotificationCompat.Builder(context!!, channelId)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("База данных")
                .setContentText("Записали")
                .build()
            App.notificationManager.notify(1, notification)
        }
    }

    fun createChannel(): NotificationChannel {
        val channel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //TODO Для версий 8.0 и выше
            NotificationChannel(
                channelId,
                "Запись в базу данных",
                NotificationManager.IMPORTANCE_HIGH
            )
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        channel.enableLights(true)
        channel.lightColor = Color.RED
        channel.enableVibration(true)
        channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 100, 200, 300, 400)
        return channel
    }

}