package com.example.myapplication

import android.app.Application
import android.app.NotificationManager
import android.content.Context

class App : Application() {

    companion object {
        lateinit var notificationManager: NotificationManager

        fun manager(notifManager: NotificationManager) {
            notificationManager = notifManager
        }

    }

    override fun onCreate() {
        super.onCreate()
        manager(getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
    }
}