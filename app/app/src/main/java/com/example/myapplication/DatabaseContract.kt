package com.example.myapplication

import android.provider.BaseColumns


object DatabaseContract {
    object UserEntity : BaseColumns {
        const val TABLE_NAME = "user"
        const val COLUMN_NAME_FIO = "fio"
        const val COLUMN_NAME_ADDRESS = "address"
        const val COLUMN_NAME_PHONE_NUMBER = "phone_number"
        const val COLUMN_NAME_GENDER = "gender"
        const val COLOR = "color"
    }

    const val SQL_CREATE_USER =
        "CREATE TABLE ${UserEntity.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${UserEntity.COLUMN_NAME_FIO} TEXT," +
                "${UserEntity.COLUMN_NAME_PHONE_NUMBER} TEXT," +
                "${UserEntity.COLUMN_NAME_ADDRESS} TEXT," +
                "${UserEntity.COLUMN_NAME_GENDER} TEXT,"+
                "${UserEntity.COLOR} TEXT)"

    const val SQL_DELETE_USER = "DROP TABLE IF EXISTS ${UserEntity.TABLE_NAME}"
}